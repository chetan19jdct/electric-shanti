
require("dotenv").config(); 
const express = require('express');
const app = express();
const formidable = require('express-formidable');
app.use(formidable());
var cors = require('cors');
var path = require('path');
app.use(cors());
const userRouter = require("./api/router/user-router");
/*  ************************** language change  *************************** */
app.use(function (req, res, next) {
	if(req.headers.language == "en") {
        global.handleRes = require('./language/lang-eng');
    } else if(req.headers.language == "hb") {
        global.handleRes = require('./language/lang-hibru');
    } else if(req.headers.language == 'ar') {
        global.handleRes = require('./language/lang-ar');
    } else {
        global.handleRes = require('./language/lang-eng');
    }    
	next();
});
/*  ************************** language change  *************************** */
app.use("/api",userRouter);
var constants = require("./config/constants"); 
global.commonController = require('./models/common_controller')({});
app.use("/upload/images", express.static(path.join(__dirname, '/upload/images')));
app.listen(process.env.APP_PORT,() => {
	console.log("servr up and running:");
}) 
