var messageCodes = {};
messageCodes[exports.SUCCESS = "success"] = "success";
messageCodes[exports.ERROR = "error"] = "error";
exports.getMessage = function (messageCode, entity = '') {
    if (messageCodes.hasOwnProperty(messageCode)) {
      if (entity) {
        return messageCodes[messageCode].replace(/%s/g, entity);
      }
      return messageCodes[messageCode];
    } else {
      throw new Error("Message code does not exist: " + messageCode);
    }
};