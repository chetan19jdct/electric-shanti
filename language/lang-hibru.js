var messageCodes = {};
messageCodes[exports.SUCCESS = "success 2"] = "success 2";
messageCodes[exports.ERROR = "error 2"] = "error 2";

exports.getMessage = function (messageCode, entity = '') {
    if (messageCodes.hasOwnProperty(messageCode)) {
      if (entity) {
        return messageCodes[messageCode].replace(/%s/g, entity);
      }
      return messageCodes[messageCode];
    } else {
      throw new Error("Message code does not exist: " + messageCode);
    }
};