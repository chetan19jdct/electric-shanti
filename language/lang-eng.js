var messageCodes = {};
messageCodes[exports.SUCCESS = "success"] = "success";
messageCodes[exports.ERROR = "error"] = "error";

messageCodes[exports.ApiKeyNotMatch = "Invalid api key 1"] = "Invalid api key 1";
messageCodes[exports.INVALID_TOKEN = "Invalid token"] = "Invalid token";


messageCodes[exports.DEVICE_ERROR = "Invalid device type"] = "Invalid device type";

messageCodes[exports.BAD_REQUEST = 200] = "Bad Request";
messageCodes[exports.OK = 200] = "OK";
messageCodes[exports.CREATED = 201] = "Created";
messageCodes[exports.ACCEPTED = 202] = "Accepted";
messageCodes[exports.NO_CONTENT = 204] = "No Content";
messageCodes[exports.BAD_REQUEST = 200] = "Bad Request";
messageCodes[exports.UNAUTHORIZED = 401] = "Unauthorized";
messageCodes[exports.ACCESS_DENIED = 403] = "Access Denied";
messageCodes[exports.RESOURCE_NOT_FOUND = 404] = "Resource was not found";
messageCodes[exports.METHOD_NOT_ALLOWED = 405] = "Method not allowed";
messageCodes[exports.CONFLICT = 409] = "Conflict";
messageCodes[exports.INTERNAL_SERVER_ERROR = 500] = "!Internal Server Error: %s";

/* login */
messageCodes[exports.EMPTY_FIELD = "Empty field"] ="Empty field";
messageCodes[exports.MOBILE_EMPTY = "Please insert mobile number"] = "Please insert mobile number";
messageCodes[exports.PASSWORD_EMPTY = "Please insert password"] ="Please insert password";
messageCodes[exports.COUNTRY_CODE_EMPTY = "Please insert country code"] = "Please insert country code";
messageCodes[exports.INACTIVE_USER = "Inactive user"] = "Inactive user";
messageCodes[exports.INVALID_LOGIN = "Invalid email or password"] = "Invalid email or password";
messageCodes[exports.SUCCESSFULL_LOGGEDIN = "logged in successfully"] =  "logged in successfully";
messageCodes[exports.FIELDMISSING = "Field missing"] =  "Field missing";

messageCodes[exports.VENDORADDED = "Vendor added successfully"] = "Vendor added successfully";
messageCodes[exports.USERADDED = "User added successfully"] = "User added successfully";
messageCodes[exports.USERDATA  = "User data"] = "User data";

messageCodes[exports.USER_NOTFOUND  = "User not found"] = "User not found";
exports.getMessage = function (messageCode, entity = '') {
    if (messageCodes.hasOwnProperty(messageCode)) {
      if (entity) {
        return messageCodes[messageCode].replace(/%s/g, entity);
      }
      return messageCodes[messageCode];
    } else {
      throw new Error("Message code does not exist: " + messageCode);
    }
};