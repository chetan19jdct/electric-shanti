let checkapikey = (req, res, next) => {
    /* api key  validation */
    if(req.headers.api_key==undefined || req.headers.api_key!=APIKEY) {
        var response={status:false,result_code:handleRes.ERROR,message:handleRes.ApiKeyNotMatch,data:{}};
        return res.send(response);
    }
    /* device type */
    if(req.headers.device_type != "ios" && req.headers.device_type != "android" && req.headers.device_type !=  "web") {
        var response={status:false,result_code:handleRes.ERROR,message:handleRes.DEVICE_ERROR,data:{}};
        return res.send(response); 
    } else {
        next();
    }
}
module.exports = { checkapikey: checkapikey }