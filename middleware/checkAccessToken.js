var modelss = require('../models/common-model');
//let checkAccessToken = async function (req, callback) {
    let checkAccessToken = (req, res, next) => {
        (async () => {
        if(req.headers.device_type == "ios" || req.headers.device_type == "android" || req.headers.device_type == "web") {
        /* api key  validation */
            var body = req.fields;
            if(req.headers.api_key==undefined || req.headers.api_key!=APIKEY) {
                var response = {status:false,result_code: handleRes.ERROR,message:handleRes.ApiKeyNotMatch,data:{}};
                return res.send(response);
            } else {
                var userid = body.user_id;
                var access_token = req.headers.access_token;
                var device_type =  req.headers.device_type;
                var device_id =  req.headers.device_id;
                var role =  body.role;
                var where = "`id`='"+userid+"' AND `admin_status`='1' AND `role` = '"+role+"'";
                var userloggedin = await modelss.selectAsc('ec_users', where, '*');
                if(userloggedin.length < 1) {
                    var response = {status:false,result_code: handleRes.ERROR, message:handleRes.USER_NOTFOUND,data:{}};
                    return res.send(response);
                } else {
                    var wheress = "`device_id` = '"+device_id+"' AND `device_type` = '"+device_type+"' AND `user_id`='"+userid+"' AND `access_token` = '"+access_token+"'";
                    var userloggedin = await modelss.selectAsc('auth_tokens', wheress, '*');
                    if(userloggedin.length < 1) {
                        var response = {status:false,result_code: handleRes.ERROR, message:handleRes.INVALID_TOKEN,data:{}};
                        return res.send(response);
                    } else {
                        next();
                    }
                }
            }
        } else {
            var response={status:false,result_code: handleRes.ERROR,message:global.handleRes.DEVICE_ERROR,data:{}};
            return res.send(response);      
        }
    })();
}
module.exports = { checkAccessToken: checkAccessToken }