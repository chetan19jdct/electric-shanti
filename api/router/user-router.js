const router = require("express").Router();
const path = require("path");
var usercontroller = require("../controller/user-controller")({});
apikeyAdmin = require('../../middleware/checkApiKey');
checkAccessToken = require('../../middleware/checkAccessToken');
router.post("/addEditVendor",apikeyAdmin.checkapikey,function(req, res, next){
    usercontroller.addEditVendor(req, (plErr, plRes, code) => {
        var hosturl = "http://"+req.get('host');
        if (plRes) {
           res.json({status: true,result_code: handleRes.SUCCESS, message: handleRes.VENDORADDED, data: plRes});
        }
        else { 
            res.status(code);
            if (plErr == null) {
                plErr = handleRes.getMessage(handleRes.SOMETHING_WENT_WRONG);
            }
            res.json({ status: false,result_code: handleRes.ERROR, message: plErr, data: {} });
        }
    });
}); 
router.post("/addEdituser",apikeyAdmin.checkapikey,function(req, res, next){
    usercontroller.addEdituser(req, (plErr, plRes, code) => {
        var hosturl = "http://"+req.get('host');
        if (plRes) {
           res.json({status: true,result_code: handleRes.SUCCESS, message: handleRes.VENDORADDED, data: plRes});
        }
        else { 
            res.status(code);
            if (plErr == null) {
                plErr = handleRes.getMessage(handleRes.SOMETHING_WENT_WRONG);
            }
            res.json({ status: false,result_code: handleRes.ERROR, message: plErr, data: {} });
        }
    });
}); 
router.post("/login",apikeyAdmin.checkapikey,function(req, res, next){
    usercontroller.login(req, (plErr, plRes, code) => {
        var hosturl = "http://"+req.get('host');
        if (plRes) {
           res.json({status: true,result_code: handleRes.SUCCESS, message: handleRes.USERADDED, data: plRes});
        }
        else { 
            res.status(code);
            if (plErr == null) {
                plErr = handleRes.getMessage(handleRes.SOMETHING_WENT_WRONG);
            }
            res.json({ status: false,result_code: handleRes.ERROR, message: plErr, data: {} });
        }
    });
}); 
router.post("/userdata",checkAccessToken.checkAccessToken,function(req, res, next){
    usercontroller.userdata(req, (plErr, plRes, code) => {
        var hosturl = "http://"+req.get('host');
        if (plRes) {
           res.json({status: true,result_code: handleRes.SUCCESS, message: handleRes.USERDATA, data: plRes});
        }
        else { 
            res.status(code);
            if (plErr == null) {
                plErr = handleRes.getMessage(handleRes.SOMETHING_WENT_WRONG);
            }
            res.json({ status: false,result_code: handleRes.ERROR, message: plErr, data: {} });
        }
    });
}); 
module.exports = router;