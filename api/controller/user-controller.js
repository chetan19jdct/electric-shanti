const fs = require('fs');
var path = require("path");
const { Validator } = require('node-input-validator');
var request = require("request");
var model = require('../../models/common-model');
var modelCustom = require('../../models/custom-model');
const { exit } = require('process');
const { forever } = require('request');
const {genSaltSync, hashSync, compareSync} = require("bcrypt");
const { sign, verify } = require("jsonwebtoken");
const e = require('express');
module.exports = (params) => {
   return {
      addEditVendor: async function (req, callback) {
         let params = req.fields;
         var role = params.role;
         var business_name = params.business_name;
         var business_representative = params.business_representative;
         var phone_number = params.phone_number;
         var email = params.email;
         var sex = params.sex;
         var birthdate = params.birthdate;
         var password = params.password;
         var country_code = params.country_code;
         var country = params.country;
         let rules = {};
         rules.role = rules.business_name =  rules.business_representative =  rules.phone_number =  rules.email =  rules.sex =  rules.birthdate =  rules.password = rules.country_code = rules.country = 'required';
         commonController.validateRequest(params, rules, async (err, res) => {
            if (err) {
               callback(err, null, handleRes.BAD_REQUEST);
            } else {
               var duplicateemail = await modelCustom.checkDuplicateData("ec_users",'email',email); 
               if(duplicateemail) {
                  callback("Duplicate Email",  null, handleRes.BAD_REQUEST);
               } else {
                  var duplicatiephone = await modelCustom.checkDuplicateData("ec_users",'phone',phone_number); 
                  if(duplicatiephone) {
                     callback("Duplicate Phone number",  null, handleRes.BAD_REQUEST);
                  } else {
                     if(role != "vendor") {
                        callback("Invalid role",  null, handleRes.BAD_REQUEST);
                     } else {
                        var insertdata = {}  
                        const salt = genSaltSync(10);
	                     password = hashSync(password, salt);  
                        let column = "role,email,country_code,phone,password,business_name,business_representative,sex,birthday,country";
                        let value = "'"+role+"','"+email+"','"+country_code+"','"+phone_number+"','"+password+"','"+business_name+"','"+business_representative+"','"+sex+"','"+birthdate+"','"+country+"'";
                        insertedata = await model.insertasc('ec_users', column, value); 
                        callback(null, {message:"Registered successfully",registereduserid: insertedata.data})
                     }
                  }
               }
            }
         })  
      },
      addEdituser: async function (req, callback) {
         let params = req.fields;
         var role = params.role;
         var first_name = params.first_name;
         var last_name = params.last_name;
         var phone_number = params.phone_number;
         var email = params.email;
         var password = params.password;
         var country_code = params.country_code;
         let rules = {};
         rules.role = rules.first_name = rules.last_name = rules.phone_number =  rules.email =  rules.password = rules.country_code = "required";
         commonController.validateRequest(params, rules, async (err, res) => {
            if (err) {
               callback(err, null, handleRes.BAD_REQUEST);
            } else {
               var duplicateemail = await modelCustom.checkDuplicateData("ec_users",'email',email); 
               if(duplicateemail) {
                  callback("Duplicate Email",  null, handleRes.BAD_REQUEST);
               } else {
                  var duplicatiephone = await modelCustom.checkDuplicateData("ec_users",'phone',phone_number); 
                  if(duplicatiephone) {
                     callback("Duplicate Phone number",  null, handleRes.BAD_REQUEST);
                  } else {
                     if(role != "user") {
                        callback("Invalid role",  null, handleRes.BAD_REQUEST);
                     } else {
                        var insertdata = {}  
                        const salt = genSaltSync(10);
	                     password = hashSync(password, salt);  
                        let column = "role,email,country_code,phone,password,firstname,lastname";
                        let value = "'"+role+"','"+email+"','"+country_code+"','"+phone_number+"','"+password+"','"+first_name+"','"+last_name+"'";
                        insertedata = await model.insertasc('ec_users', column, value); 
                        callback(null, {message:"Registered successfully",registereduserid: insertedata.data})
                     }
                  }
               }
            }
         })  
      },
      login: async function (req, callback) {
         let params = req.fields;
         var role = params.role;
         var email = params.email;
         var password = params.password;
         let rules = {};
         rules.role = rules.email = rules.password = "required";
         commonController.validateRequest(params, rules, async (err, res) => {
            if (err) {
               callback(err, null, handleRes.BAD_REQUEST);
            } else {
               where = "email = '"+email+"' AND role = '"+role+"'"; 
               var userloggedin = await model.selectAsc('ec_users', where, '*');
               if(userloggedin.length < 1) {
                  callback(handleRes.INVALID_LOGIN,  null, handleRes.BAD_REQUEST);
               } else {
                  var userpassSaved =  userloggedin[0].password
                  const resultpass = compareSync(password, userpassSaved);
                  if(!resultpass) {
                     callback(handleRes.INVALID_LOGIN,  null, handleRes.BAD_REQUEST);
                  } else {
                    userid = userloggedin[0].id;
                    device_type = req.headers.device_type;
                    device_id = req.headers.device_id;
                    var c_date = created_at =  new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    const ui= {'id':userid};
                     expiresIn = 3600000,
                     jwtId = Math.random().toString(36).substring(7);
                     var payload = {							
                        expiresIn:expiresIn,
                        jwtid : jwtId,
                        audience : 'TEST'
                     };	
                     var jwttoken = sign({ui}, JWTKEY,payload);
                     responsedata = {token: jwttoken,userdata: userloggedin}
                     var where = "`device_id` = '"+device_id+"' AND `device_type` = '"+device_type+"' AND `user_id`='"+userid+"' ";
                     var result = await model.selectAsc('auth_tokens', where, '*'); 
                     if(result.length > 0) {
                        var where = "`user_id` = '"+userid+"' AND `device_id` = '"+device_id+"' AND `device_type` = '"+device_type+"'";
                        var values = "`access_token`= '"+jwttoken+"', `device_id` = '"+device_id+"' , `device_type` = '"+device_type+"' , `created_at` = '"+created_at+"', `last_access` = '"+created_at+"',`updated_at` = '"+created_at+"'";	 ; 	 
                        await model.updateasc('auth_tokens',values,where);  
                     } else {
                        var column   = "user_id,device_id,device_type,access_token,created_at,last_access,updated_at";
                        var value =  "'"+userid+"','"+device_id+"','"+device_type+"','"+jwttoken+"','"+created_at+"','"+created_at+"','"+created_at+"'";
                        await model.insertasc('auth_tokens', column, value); 
                     }
                     callback(null, {message:"logged in success", data : responsedata})
                  }
               }
            }   
         })
      },
      userdata: async function (req, callback) {
         callback(null, {message:"user data", data : "user data"})
      }
   }
}