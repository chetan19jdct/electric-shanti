const { json } = require('express');
const { Validator } = require('node-input-validator');
var abc = require('node-input-validator');
module.exports = (params) => {
  return {
    validateRequest: (req, rules, callback) => {
      /*
      check_one_by_one it should be set at controller field parameter 
      to speicfy that do you want to check with each field or 
      at one short
      */
      let r = {};   
      let chkvalidator = new Validator(req, rules);
      chkvalidator.check().then(function (matched) {
        if (Object.keys(chkvalidator.errors).length != 0) {
          if(req.check_one_by_one) {
                var errorfieldkey = Object.keys(chkvalidator.errors)[0];
                var msg = chkvalidator.errors[errorfieldkey].message;
                callback(msg, null);
          } else {
              callback(handleRes.FIELDMISSING, null);
          }
       } else {
          callback(null, true);
        }
      });
    },
  };
};
