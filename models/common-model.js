var connection = require('../config/database');
var fs = require("fs");
var mv = require('mv');
const { DefaultSerializer } = require('v8');
function Transaction() {
  /******************************/
  //get all record
  this.getname = async function(id, key){
    // if(key == "package") {
    //   var query = 'SELECT `pkg_name` FROM ' + PACKAGES + ' where ' + where + '`id` = "'+id+'"' ;
    //   var res = await connection.query(query);
    //   return true;
    // } else {
    //   return false;
    // }
    return true; 
  }
  this.selectAsc = async function (table, where, select, orderby = 'order by id desc') {
    if (where == '') where = 1;
    var query = 'SELECT ' + select + ' FROM ' + table + ' where ' + where + ' ' + orderby;
  
    console.log(query);
    var res = await connection.query(query);
    //~ return res.rows;
    return res;
  };
  this.getImagePath = async function(keyid,key) {
    where = "`key_id` = '"+keyid+"' AND `media_key` = '"+key+"'";
    var query = 'SELECT `path` FROM ' + MEDIA + ' where ' + where ;
    var res = await connection.query(query);
    return res;
  };
  this.selectAscLimit = async function (table, where, select, order = '', limit = 0, offset = 0) {
    if (where == '') where = 1;
    var query = 'SELECT ' + select + ' FROM ' + table + ' where ' + where + ' ' + order + ' LIMIT ' + limit + ' OFFSET ' + offset;
    var res = await connection.query(query);
    return res;
  };
 
  this.selectRowasc = async function (table, where, select) {
    if (where == '') where = 1;
    var query = 'SELECT ' + select + ' FROM ' + table + ' where ' + where;
   console.log("aaa1");
    var res = await connection.query(query);
    console.log("aaa2");
    //~ return res.rows[0];
    return res;
  };
  
  this.selectRowOrderby = async function (table, where, select, orderby = 'order by id desc') {
    if (where == '') where = 1;
    var query = 'SELECT ' + select + ' FROM ' + table + ' where '  + where + ' ' + orderby;
    console.log(query);
    var res = await connection.query(query);
    //~ return res.rows[0];
    return res;
  };

  this.insertasc = async function (table, column, value) {
    let query = "INSERT INTO "+table+"("+column+")  VALUES ("+value+")";
    console.log(query);
    var res = await connection.query(query);
    if (res) {
      return ({ status: 1, data: res.insertId });
    } else {
      return ({ status: 1, data: 0 });
    }
  };

  this.updateasc = async function (table, value, where) {
    let query = "UPDATE " + table + " SET " + value + " WHERE " + where;
    console.log("UPDATE", query);
    var res = await connection.query(query);
    if (res) {
      return ({ status: 1, data: res });
    } else {
      console.log(res);
      return ({ status: 0, data: res });
    }
  };

  // delete data in table
  this.deleteasc = async function (table, where) {
    let query = "DELETE FROM " + table + " WHERE " + where;
    console.log(query);
    var res = await connection.query(query);
    if (res) {
      return ({ status: 1, data: res });
    } else {
      return ({ status: 0, data: res });
    }

  };

  // custom query
  this.customAwaitSelect = async function (query) {
    
    var res = await connection.query(query);
     return res;
  };
  
  // custom query
  this.customAwait = async function (query) {
    console.log(query);
    var res = await connection.query(query);
    return res;
  };

  this.insertAwait = async function (table, column, value) {
    let query = "INSERT INTO " + table + "(" + column + ")  VALUES "+value+"";
    //INSERT INTO "users" ("email") VALUES ('test1@example.com'), ('test2@example.com')

    console.log(query);
    var res = await connection.query(query);
    if (res) {
      return ({ status: 1, data: res });
    } else {
      return ({ status: 1, data: res });
    }
  };

  /*this.insertAwait=async function (table,column,value)
   {
         // let query = "INSERT INTO "+table+"("+column+")  VALUES ("+value+")";

         let query = "INSERT INTO "+table+"("+column+")  VALUES ("+value+") RETURNING id";
    console.log(query);
    var res = await connection.query(query);
    if (res) {
      return ({ status: 1, data: res });
    } else {
      return ({ status: 1, data: res });
    }
          
           let query = "INSERT INTO "+table+"("+column+")  VALUES ?";
          console.log(query);
          
         var res= await connection.query(query,[value]);
          console.log(res);
          if(res){
          
           return ({status:1,data:res});
          }else{
            console.log(result);
                    return ({status:1,data:res});
            }
   };*/

  this.updateAwait = async function (table, value, where) {
    // update statment
    let query = "UPDATE " + table + " SET " + value + " WHERE " + where;
    console.log(query);
    var res = await connection.query(query);
    if (res) {
      return ({ status: 1, data: res });
    } else {
      return ({ status: 0, data: res });
    }
  };

  // delete data in table
  this.deleteAwait = async function (table, whereCondition) {
    let query = "DELETE FROM " + table + " WHERE " + whereCondition;
   // console.log(query);
    var res = await connection.query(query);
    if (res) {
      return ({ status: 1, data: res });
    } else {
      return ({ status: 0, data: res });
    }
  };

  //check access token
  this.CheckAccessToken = function (table, data, callback) {
    if (data.access_token != undefined && data.access_token != '') {
      var where = "`device_id` = '" + data.device_id + "' and `device_type`='" + data.device_type + "' and `access_token`='" + data.access_token + "'";
    }
    else {
      var where = "`device_id` = '" + data.device_id + "' and `device_type`='" + data.device_type + "'";
    }
    var query = 'SELECT * FROM ' + table + ' where ' + where;
    // console.log(query);
    connection.query(query, function (err, result, field) {
      if (err) {
        callback({ status: 2, err: err });
      }
      else {
        callback({ status: 1, data: result });
      }
    });
  };

  /*******************************************************/

  this.joinTwoWithWhereAscLimit = async function (select, tbl1, tbl2, feild1, feild2, where, orderby = '', limit = 0, offset = 0) {
    let query = "SELECT " + select + " FROM " + tbl1 + " JOIN " + tbl2 + " ON " + tbl1 + "." + feild1 + " = " + tbl2 + "." + feild2 + " WHERE " + where + orderby + " LIMIT " + limit + " OFFSET " + offset + "";
    console.log(query);
    var res = await connection.query(query);
    return res;
  };

  this.joinTwoWithWhereAsc = async function (select, tbl1, tbl2, feild1, feild2, where, orderby = '') {
    let query = "SELECT " + select + " FROM " + tbl1 + " JOIN " + tbl2 + " ON " + tbl1 + "." + feild1 + " = " + tbl2 + "." + feild2 + " WHERE " + where + " " + orderby + "";
    console.log(query);
    var res = await connection.query(query);
    return res.rows[0];
    //return res;
  };

  this.joinTwoWithWhereOrderAsc = async function (select, tbl1, tbl2, feild1, feild2, where, orderby = 'id') {
    if (orderby == '') { orderby = ['id']; }
    order = orderby.join(',');
    let query = "SELECT " + select + " FROM " + tbl1 + " JOIN " + tbl2 + " ON " + tbl1 + "." + feild1 + " = " + tbl2 + "." + feild2 + " WHERE " + where + " ORDER BY " + order + "";
    // console.log(query);
    var res = await connection.query(query);
    return res;
  };

  this.joinLeftTwoWithWhereAsc = async function (select, tbl1, tbl2, feild1, feild2, where, orderby) {
    let query = "SELECT " + select + " FROM " + tbl1 + " LEFT JOIN " + tbl2 + " ON " + tbl1 + "." + feild1 + " = " + tbl2 + "." + feild2 + " WHERE " + where + orderby + "";
    // console.log(query);
    var res = await connection.query(query);
    return res;
  };

  this.joinThreeWithWhereAsc = async function (select, tbl1, tbl2, tbl3, feild1, feild2, feild3, where, orderby = '', limit = 0, offset = 0) {
    let query = "SELECT " + select + " FROM " + tbl1 + " INNER JOIN " + tbl2 + " ON " + tbl1 + "." + feild1 + " = " + tbl2 + "." + feild2 + " INNER JOIN " + tbl3 + " ON " + tbl1 + "." + feild1 + " = " + tbl3 + "." + feild3 + " WHERE " + where + orderby + " LIMIT " + limit + " OFFSET " + offset + "";
    // console.log(query);
    var res = await connection.query(query);
    return res;
  };
    /* custom functions cs */
    /* image upload */
  this.uploadimageSingle = async function(uploadDir,keyid,keyname,imagetype, filename,tmppath) {
    var newPath = uploadDir + Date.now()+"_" +filename;
    
    //fs.rename(tmppath, newPath,(errorRename) => {})
    mv(tmppath, newPath, function(err) {	 
      if (err) console.log(err);
    });

    var column   = "media_key,key_id,extention,path";
    //var value = [[keyname,keyid,imagetype,newPath]];
    
    var value =  '"'+keyname+'","'+keyid+'","'+imagetype+'","'+newPath+'"'; 
    //var value1 = 
    table = MEDIA;
    let query = "INSERT INTO " + table + "(" + column + ")  VALUES (" + value +")";
        var res = await connection.query(query);
      if (res) {
        return ({ status: 1, data: res.insertId });
      } else {
        return ({ status: 1, data: res });
      }
  }
}

//create condition for where AND or OR
function createCondition(where, operator) {
  var html = [];
  for (let [key, value] of Object.entries(where)) {
    html.push('`' + key + "`='" + value + "'");
  }
  return html.join(' ' + operator + ' ');
}

module.exports = new Transaction();
