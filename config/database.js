const util = require('util');
const mysql = require('mysql');
var config = {
	port: process.env.DB_PORT,
	host: process.env.DB_HOST,
	user: process.env.DB_USER,
	password: process.env.DB_PASS,
	database: process.env.MYSQL_DB,
	connectionLimit: 10
};
function makeDb(config) {
  const connection = mysql.createConnection(config); return {
    query(sql, args) {
      return util.promisify(connection.query)
        .call(connection, sql, args);
    },
    close() {
      return util.promisify(connection.end).call(connection);
    }
  };
}
const db = makeDb(config);
module.exports = db;